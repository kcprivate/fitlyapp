import React, {useEffect} from 'react';
import {Text} from 'react-native';

import ScrollPicker from 'react-native-wheel-scrollview-picker';
import styled from 'styled-components';

export const ScrollOption = ({type, data, changeItem, loadComponent}) => {
  useEffect(() => {
    loadComponent();
  }, []);

  const renderItem = data => {
    return (
      <Item>
        <Text style={{fontSize: 40}}>{data}</Text>
      </Item>
    );
  };

  const renderScroll = () => {
    return (
      <ScrollPicker
        dataSource={data}
        selectedIndex={0}
        itemHeight={60}
        wrapperColor="transparent"
        highlightColor="#005b96"
        renderItem={renderItem}
        onValueChange={changeItem}
      />
    );
  };

  return (
    <ScreenWrapper>
      {type === 'height' || type === 'weight' ? (
        <UnitWrapper>
          {renderScroll()}
          <Unit>
            {(type === 'height' && 'cm') || (type === 'weight' && 'kg')}
          </Unit>
        </UnitWrapper>
      ) : (
        renderScroll()
      )}
      <ItemShadow style={{top: 0}} />
      <ItemShadow style={{bottom: 0}} bottom />
    </ScreenWrapper>
  );
};

const ScreenWrapper = styled.View`
  width: 100%;
  height: 50%;
  margin-top: 70px;
  padding-bottom: 25px;
  padding-horizontal: 50px;
`;

const ItemShadow = styled.View`
  position: absolute;
  width: 100%;
  opacity: 0.75;
  height: ${p => (p.bottom ? '85px' : '55px')};
  background-color: #f0f0f0;
  align-self: center;
`;

const Item = styled.View`
  justify-content: center;
  align-items: center;
`;

const UnitWrapper = styled.View`
  width: 100%;
  height: 100%;
  flex-direction: row;
  align-items: center;
`;

const Unit = styled.Text`
  font-size: 25px;
  margin-left: 10px;
`;
