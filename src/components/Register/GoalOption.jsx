import React, {useEffect} from 'react';

import styled from 'styled-components';

export const GoalOption = ({selected, setSelected, setTitle}) => {
  useEffect(() => {
    setTitle('What is your goal?');
  }, []);

  return (
    <GoalOptionView>
      <GoalOptionWrapper
        style={{
          shadowOffset: {
            width: 0,
            height: 2,
          },
          elevation: 4,
        }}
        selected={selected === 0}
        onPress={() => setSelected(0)}>
        <GoalOptionText selected={selected === 0} title>
          Be healthier
        </GoalOptionText>
        <GoalOptionText selected={selected === 0} secondText>
          Get leaner and increase your strength
        </GoalOptionText>
      </GoalOptionWrapper>
      <GoalOptionWrapper
        style={{
          shadowOffset: {
            width: 0,
            height: 2,
          },
          elevation: 4,
        }}
        selected={selected === 1}
        onPress={() => setSelected(1)}>
        <GoalOptionText selected={selected === 1} title>
          Lose weight
        </GoalOptionText>
        <GoalOptionText selected={selected === 1} secondText>
          Eat and train for optimum health
        </GoalOptionText>
      </GoalOptionWrapper>
      <GoalOptionWrapper
        style={{
          shadowOffset: {
            width: 0,
            height: 2,
          },
          elevation: 4,
        }}
        selected={selected === 2}
        onPress={() => setSelected(2)}>
        <GoalOptionText selected={selected === 2} title>
          Gain weight
        </GoalOptionText>
        <GoalOptionText selected={selected === 2} secondText>
          Build muscle strength
        </GoalOptionText>
      </GoalOptionWrapper>
    </GoalOptionView>
  );
};

const GoalOptionView = styled.View`
  width: 100%;
  height: 50%;
  margin-top: 70px;
`;

const GoalOptionWrapper = styled.TouchableOpacity`
  background-color: ${p => (p.selected ? '#005b96' : '#ffffff')};
  border-color: ${p => (p.selected ? '#005b96' : '#ffffff')};
  border-width: 1px;
  padding: 20px;
  border-radius: 15px;
  border-top-right-radius: 35px;
  border-bottom-right-radius: 0px;
  margin-vertical: 10px;
  shadow-color: #000;
  shadow-opacity: 0.23;
  shadow-radius: 2.62px;
`;

const GoalOptionText = styled.Text`
  color: ${p =>
    p.selected
      ? p.secondText
        ? '#999999'
        : '#ffffff'
      : p.secondText
      ? 'gray'
      : '#005b96'};
  font-size: ${p => (p.title ? '20px' : '10px')};
  margin-bottom: ${p => (p.title ? '5px' : '0px')};
`;
