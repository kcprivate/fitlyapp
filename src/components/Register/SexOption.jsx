import React, {useEffect} from 'react';

import styled from 'styled-components';

import Men from '@images/icons/men.svg';
import Women from '@images/icons/women.svg';

export const SexOption = ({
  selected,
  setSelected,
  setTitle,
  setDescription,
}) => {
  useEffect(() => {
    setTitle('Male of Female?');
    setDescription('Men and women need diffrent nutrition approaches');
  }, []);

  return (
    <SexOptionWrapper>
      <SexOptionSubWrapper>
        <SexWrapper onPress={() => setSelected(0)} selectedSex={selected === 0}>
          <Men
            width={100}
            height={100}
            fill={selected === 0 ? 'white' : 'black'}
            strokeWidth="0"
            stroke="black"
          />
          <SexTextWrapper selectedSex={selected === 0}>Male</SexTextWrapper>
        </SexWrapper>
        <SexWrapper onPress={() => setSelected(1)} selectedSex={selected === 1}>
          <Women
            width={100}
            height={100}
            fill={selected === 1 ? 'white' : 'black'}
            strokeWidth="0"
            stroke="black"
          />
          <SexTextWrapper selectedSex={selected === 1}>Female</SexTextWrapper>
        </SexWrapper>
      </SexOptionSubWrapper>
    </SexOptionWrapper>
  );
};

const SexOptionWrapper = styled.View`
  width: 100%;
  height: 50%;
  margin-top: 70px;
`;

const SexOptionSubWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const SexWrapper = styled.TouchableOpacity`
  padding-vertical: 50px;
  padding-horizontal: 20px;
  background-color: ${p => (p.selectedSex ? '#005b96' : '#ffffff')};
  border-radius: 15px;
`;

const SexTextWrapper = styled.Text`
  color: ${p => (p.selectedSex ? 'white' : '#005b96')};
  font-weight: 500;
  font-size: 20px;
  text-align: center;
  margin-top: 10px;
`;
