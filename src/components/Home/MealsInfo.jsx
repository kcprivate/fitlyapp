import React, {useState} from 'react';
import {View, ScrollView} from 'react-native';

import styled from 'styled-components';

import Add from '@images/icons/add.svg';
import {SectionHeader} from './SectionHeader';

import {vh, vw} from '@utils/sizes';

export const MealsInfo = () => {
  const [meals, setMeals] = useState([
    [
      'Breakfast',
      'Bread,\n Peanut butter,\n Apple',
      525,
      '#fa7b82',
      require('@images/breakfast.png'),
    ],
    [
      'Lunch',
      'Salmon,\n Mixed veggies,\n Avocado',
      300,
      '#656fe2',
      require('@images/lunch.png'),
    ],
    [
      'Dinner',
      'Chicken,\n Potatos,\n Tomato',
      1000,
      '#fac15a',
      require('@images/dinner.png'),
    ],
    ['Tea Time', '', 0, '#1bf4db', require('@images/breakfast.png'), 400],
    ['Snacks', '', 0, '#ff5e90', require('@images/breakfast.png'), 100],
    ['Evening meal', '', 0, '#8b00ee', require('@images/breakfast.png'), 1000],
  ]);

  const renderMeal = (type, desc, kcal, color, icon, recommendedKcal) => {
    return (
      <ItemWrapper
        vh={vh()}
        vw={vw()}
        onPress={() => console.log('clicked meal')}>
        <ItemImage source={icon} />
        <MealWrapper color={color}>
          <ImageShadow />
          <MealName>{type}</MealName>
          {desc === '' && kcal === 0 ? (
            <>
              <NoMealsTitle>Recommended</NoMealsTitle>
              <NoMealsKcal>{recommendedKcal + ' kcal'}</NoMealsKcal>
              <NoMealsAddButton
                onPress={() => console.debug('add new meal clicked')}>
                <Add width={50} height={50} />
              </NoMealsAddButton>
            </>
          ) : (
            <>
              <MealDesc>{desc}</MealDesc>
              <KcalWrapper>
                <KcalCount>{kcal}</KcalCount>
                <KcalText>kcal</KcalText>
              </KcalWrapper>
            </>
          )}
        </MealWrapper>
      </ItemWrapper>
    );
  };

  return (
    <View style={{paddingLeft: 30}}>
      <SectionHeader
        title="Meals today"
        subtitle="Customize"
        onPress={() => console.debug('customize clicked')}
        style={{marginRight: 30}}
      />
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{paddingVertical: 30}}>
        {meals.map(e => {
          return renderMeal(e[0], e[1], e[2], e[3], e[4], e[5] && e[5]);
        })}
      </ScrollView>
    </View>
  );
};

const ItemWrapper = styled.TouchableOpacity`
  flex: 1;
  height: ${p => p.vh / 4};
  width: ${p => p.vw / 3};
  margin-right: 20px;
`;

const ItemImage = styled.Image`
  width: 70px;
  height: 70px;
  position: absolute;
  z-index: 99;
  top: -30px;
  margin-left: 10px;
`;

const MealWrapper = styled.View`
  height: 100%;
  background-color: ${p => p.color};
  border-radius: 20px;
  padding: 5px;
  border-top-right-radius: 70px;
  justify-content: flex-end;
  align-items: center;
`;

const ImageShadow = styled.View`
  background-color: #eef1fa;
  width: 100px;
  height: 100px;
  position: absolute;
  border-radius: 50px;
  left: -7;
  top: -50;
  opacity: 0.5;
`;

const MealName = styled.Text`
  font-size: 20px;
  font-weight: 600;
  color: white;
  margin-bottom: 10px;
  text-align: center;
`;

const NoMealsTitle = styled.Text`
  color: white;
  font-weight: 500;
`;

const NoMealsKcal = styled.Text`
  color: white;
  font-size: 10px;
`;

const NoMealsAddButton = styled.TouchableOpacity`
  margin-top: 5px;
  padding: 5px;
  background-color: white;
  border-radius: 50px;
  margin-bottom: 20px;
`;

const MealDesc = styled.Text`
  font-size: 10px;
  font-weight: 400;
  color: white;
  text-align: center;
`;

const KcalWrapper = styled.View`
  flex-direction: row;
  margin-top: 20px;
  margin-bottom: 20px;
`;

const KcalCount = styled.Text`
  font-size: 30px;
  font-weight: 400;
  color: white;
`;

const KcalText = styled.Text`
  align-self: flex-end;
  margin-bottom: 3px;
  color: white;
`;
