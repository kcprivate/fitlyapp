import React from 'react';

import styled from 'styled-components';

import Forward from '@images/icons/forward.svg';

export const SectionHeader = ({title, subtitle, onPress, style}) => {
  return (
    <TitleWrapper style={style}>
      <Title>{title}</Title>
      <TitleDetailsWrapper onPress={onPress}>
        <TitleDetails>{subtitle}</TitleDetails>
        <Forward width={17} height={17} />
      </TitleDetailsWrapper>
    </TitleWrapper>
  );
};

const TitleWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 15px;
`;

const Title = styled.Text`
  font-size: 20;
  font-weight: 500;
  color: #111;
`;

const TitleDetailsWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;

const TitleDetails = styled.Text`
  font-size: 17px;
  color: #b3bff3;
  margin-right: 5px;
`;
