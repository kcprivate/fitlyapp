import React, {useState} from 'react';
import {View} from 'react-native';

import styled from 'styled-components';

import {SectionHeader} from './SectionHeader';

import {vh} from '@utils/sizes';

export const BodyInfo = () => {
  const [currentWeight, setCurrentWeight] = useState(90);
  const [kgGoal, setKgGoal] = useState(60);
  const [weeksGoal, setWeeksGoal] = useState(30);

  return (
    <View style={{paddingHorizontal: 30}}>
      <SectionHeader
        title="Body measurement"
        subtitle="Today"
        onPress={() => console.debug('clicked today button')}
      />
      <SectionWrapper
        vh={vh()}
        onPress={() => console.debug('body measurement clicked')}>
        <WeightWrapper>
          <WeightTitle>Current Weight:</WeightTitle>
          <WeightSubtitle>{currentWeight + ' Kg'}</WeightSubtitle>
        </WeightWrapper>
        <GoalsWrapper>
          <View>
            <GoalTitle>{weeksGoal + ' weeks'}</GoalTitle>
            <GoalSubtitle>left to reach your</GoalSubtitle>
          </View>
          <View>
            <GoalTitle>{kgGoal + ' Kg'}</GoalTitle>
            <GoalSubtitle>goal</GoalSubtitle>
          </View>
        </GoalsWrapper>
      </SectionWrapper>
    </View>
  );
};

const SectionWrapper = styled.TouchableOpacity`
  height: ${p => p.vh / 4 + 'px'};
  width: 100%;
  background-color: white;
  border-radius: 20px;
  border-top-right-radius: 80px;
  padding: 30px;
  justify-content: center;
`;

const WeightWrapper = styled.View`
  border-bottom-width: 1px;
  border-color: #eff0f5;
  padding-bottom: 10px;
`;

const WeightTitle = styled.Text`
  font-size: 15;
  color: #abaaaf;
`;

const WeightSubtitle = styled.Text`
  font-size: 30px;
  font-weight: 600;
  color: #718eec;
`;

const GoalsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;

const GoalTitle = styled.Text`
  font-size: 30px;
  color: #4f52dd;
`;

const GoalSubtitle = styled.Text`
  color: #abaaaf;
  text-align: center;
`;
