import React from 'react';
import {View, Text} from 'react-native';

import styled from 'styled-components';

export const RoundedButton = props => {
  return (
    <View style={{marginVertical: 5}}>
      <ButtonBackground opacity={props.opacity} bg={props.bgColor} />
      <Button color={props.bgColor} onPress={props.onPress}>
        <Text style={{color: 'white'}}>{props.text}</Text>
      </Button>
    </View>
  );
};

const ButtonBackground = styled.View`
  width: 100%;
  opacity: ${p => p.opacity || 1};
  background-color: ${p => p.bg};
  border-radius: 50px;
  padding-vertical: 30px;
`;

const Button = styled.TouchableOpacity`
  position: absolute;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  border-color: ${p => p.color};
  border-width: 1px;
  border-radius: 50px;
`;
