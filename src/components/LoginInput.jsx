import React from 'react';

import styled from 'styled-components';

export const LoginInput = props => {
  return (
    <InputWrapper half={props.half}>
      <LoginType>{props.type}</LoginType>
      <LoginField
        secureTextEntry={props.type === 'Password'}
        onChangeText={props.getValues}
        style={{
          shadowOffset: {
            width: 0,
            height: 2,
          },
          elevation: 4,
        }}
      />
    </InputWrapper>
  );
};

const InputWrapper = styled.View`
  width: ${p => (p.half ? '47%' : '100%')};
  margin-horizontal: ${p => (p.half ? '10px' : '0px')};
  margin-bottom: 15px;
`;

const LoginType = styled.Text`
  margin-left: 20px;
  margin-bottom: 5px;
  color: #777777;
`;

const LoginField = styled.TextInput`
  width: 100%;
  padding: 20px;
  border-radius: 50px;
  background-color: white;
  shadow-color: #000;
  shadow-opacity: 0.23;
  shadow-radius: 2.62px;
`;
