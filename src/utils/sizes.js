import {Dimensions} from 'react-native';

export const vw = () => {
  return Dimensions.get('window').width;
};

export const vh = () => {
  return Dimensions.get('window').height;
};
