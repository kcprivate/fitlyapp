import {createSlice} from '@reduxjs/toolkit';

export const dailyUse = createSlice({
  name: 'dailyUse',
  initialState: {
    energy: 0,
    protein: 0,
    fats: 0,
    carbons: 0,
    sugar: 0,
  },
  reducers: {
    addAll: (state, action) => {
      console.log('hejka reducer', state, action);
    },
    addEnergy: (state, action) => {
      state.energy += action.payload;
    },
    addProtein: (state, action) => {
      state.protein += action.payload;
    },
    addFats: (state, action) => {
      state.fats += action.payload;
    },
    addCarbons: (state, action) => {
      state.carbons += action.payload;
    },
    addSugar: (state, action) => {
      state.sugar += action.payload;
    },
  },
});

export const {addEnergy, addProtein, addFats, addCarbons, addSugar, addAll} =
  dailyUse.actions;

export default dailyUse.reducer;

export const usage = state => state.dailyUse;
