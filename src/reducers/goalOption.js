import {createSlice} from '@reduxjs/toolkit';

export const goalOption = createSlice({
  name: 'goal',
  initialState: {
    value: 0,
  },
  reducers: {
    change: (state, action) => {
      console.debug('hejka reducer', action.payload);
      state.value = action.payload;
    },
  },
});

export const {change} = goalOption.actions;

export default goalOption.reducer;
