import {createSlice} from '@reduxjs/toolkit';
import uuid from 'react-native-uuid';

export const productsStore = createSlice({
  name: 'products',
  initialState: {
    products: [],
  },
  reducers: {
    addProduct: (state, action) => {
      let temp = state.products;
      let data = action.payload;
      data.id = uuid.v4();
      temp.push(action.payload);
      state.products = temp;
    },
  },
});

export const {addProduct} = productsStore.actions;

export default productsStore.reducer;

export const products = state => state.products.products;
