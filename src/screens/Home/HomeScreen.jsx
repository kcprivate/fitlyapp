import React, {useEffect, useState} from 'react';
import {View, SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';

import DateTimePickerModal from 'react-native-modal-datetime-picker';
import styled from 'styled-components';

import ChevronLeft from '@images/icons/chevronLeft.svg';
import ChevronRight from '@images/icons/chevronRight.svg';
import Calendar from '@images/icons/calendar.svg';

import {DietInfo, MealsInfo, BodyInfo} from '@components';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export const HomeScreen = props => {
  const [modalVisible, setModalVisible] = useState(false);
  const [choosedDate, setChoosedDate] = useState([]);

  useEffect(() => {
    if (choosedDate.length === 0) {
      const actualDate = new Date();
      let tempArray = [];
      tempArray.push(actualDate.getDate());
      tempArray.push(months[actualDate.getMonth()]);
      tempArray.push(actualDate);

      setChoosedDate(tempArray);
    }
  }, []);

  const changeDate = type => {
    let tempDate = [...choosedDate][2];
    let dayBefore = new Date(tempDate.getTime());
    if (type === 'next') {
      dayBefore.setDate(tempDate.getDate() + 1);
    }
    if (type === 'before') {
      dayBefore.setDate(tempDate.getDate() - 1);
    }
    let resultDateArray = [];
    resultDateArray.push(dayBefore.getDate());
    resultDateArray.push(months[dayBefore.getMonth()]);
    resultDateArray.push(dayBefore);

    setChoosedDate(resultDateArray);
  };

  const changeDateFromModal = data => {
    let tempDate = data;
    let resultDateArray = [];

    resultDateArray.push(tempDate.getDate());
    resultDateArray.push(months[tempDate.getMonth()]);
    resultDateArray.push(tempDate);

    setChoosedDate(resultDateArray);
  };

  return (
    <HomeScreenWrapper>
      <SafeAreaView style={{flex: 1}}>
        <DatePickerWrapper>
          <DateTimePickerModal
            isVisible={modalVisible}
            onCancel={() => setModalVisible(false)}
            onConfirm={data => {
              changeDateFromModal(data);
              setModalVisible(false);
            }}
            mode="date"
          />
        </DatePickerWrapper>
        <ScrollView>
          <HeaderWrapper>
            <HeaderTitle>My Diary</HeaderTitle>
            <CalendarNavWrapper>
              <TouchableOpacity onPress={() => changeDate('before')}>
                <ChevronLeft height={35} width={35} fill={'#555'} />
              </TouchableOpacity>
              <CalendarNavWrapper onPress={setModalVisible}>
                <Calendar width={25} height={25} />
                <DateWrapper>
                  {choosedDate.length !== 0 &&
                    choosedDate[0] + ' ' + choosedDate[1].slice(0, 3)}
                </DateWrapper>
              </CalendarNavWrapper>
              <TouchableOpacity onPress={() => changeDate('next')}>
                <ChevronRight height={35} width={35} fill={'#555'} />
              </TouchableOpacity>
            </CalendarNavWrapper>
          </HeaderWrapper>
          <View>
            <DietInfo />
            <MealsInfo />
            <BodyInfo />
          </View>
        </ScrollView>
      </SafeAreaView>
    </HomeScreenWrapper>
  );
};

const HomeScreenWrapper = styled.View`
  flex: 1;
  background-color: #eef1fa;
`;

const DatePickerWrapper = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  justify-content: center;
  align-items: center;
`;

const HeaderWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-horizontal: 20px;
  padding-top: 15px;
`;

const HeaderTitle = styled.Text`
  color: #111;
  font-size: 30px;
  font-weight: 600;
`;

const CalendarNavWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

const DateWrapper = styled.Text`
  margin-left: 3px;
  color: #555;
`;
