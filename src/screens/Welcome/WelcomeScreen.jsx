import React, {useEffect} from 'react';
import {View, ImageBackground} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import {change} from '@src/reducers/goalOption';

import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';

import {RoundedButton} from '@components';

import {vh, vw} from '@utils/sizes';

export const WelcomeScreen = ({navigation}) => {
  let goals = useSelector(state => state.goals.value);
  const dispatch = useDispatch();

  useEffect(() => {
    console.debug('hejka', goals);
    dispatch(change((goals -= 1)));
    console.debug('hejka', goals);
  }, []);

  return (
    <ImageBackground
      source={require('@images/welcomeBG.png')}
      resizeMode={'cover'}
      style={{flex: 1}}>
      <LinearGradient
        style={{flex: 1, opacity: 0.75}}
        colors={['#2A1AD8', '#4E26E2', '#7231EC', '#953DF5', '#B948FF']}
      />
      <Content vw={vw()} vh={vh()}>
        <ContentWrapper>
          <View>
            <LogoWrapper>
              <Logo source={require('@images/logo.png')} />
            </LogoWrapper>
            <AppName>Fitly</AppName>
          </View>
          <View>
            <AppDescriptionTitle>Welcome</AppDescriptionTitle>
            <AppDescriptionSubtitle>
              to the best diet and training app
            </AppDescriptionSubtitle>
          </View>
          <View>
            <RoundedButton
              text="Sign up"
              bgColor="#005b96"
              onPress={() => navigation.navigate('RegisterOptions')}
            />
            <RoundedButton
              text="Sign in"
              bgColor="#ffffff"
              opacity={0.3}
              onPress={() => navigation.navigate('Login')}
            />
          </View>
        </ContentWrapper>
      </Content>
    </ImageBackground>
  );
};

const Content = styled.SafeAreaView`
  position: absolute;
  width: ${p => p.vw + 'px'};
  height: ${p => p.vh + 'px'};
`;

const ContentWrapper = styled.View`
  flex: 1;
  margin-vertical: 60px;
  margin-horizontal: 30px;
  justify-content: space-between;
`;

const AppName = styled.Text`
  font-size: 50px;
  color: white;
  font-weight: 700;
`;

const AppDescriptionTitle = styled.Text`
  color: white;
  font-size: 30px;
  font-weight: 500;
`;

const AppDescriptionSubtitle = styled.Text`
  color: white;
  font-size: 20px;
  margin-right: 100px;
  letter-spacing: 2px;
`;

const LogoWrapper = styled.View`
  width: 60px;
  height: 60px;
  border-radius: 10px;
  background-color: white;
  padding: 10px;
`;

const Logo = styled.Image`
  width: 100%;
  height: 100%;
`;
