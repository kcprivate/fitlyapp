import React, {useState} from 'react';
import {View, Text} from 'react-native';

import styled from 'styled-components';

import {
  RoundedButton,
  RoundedHeader,
  LoginInput,
  LoggingModal,
  SocialLogin,
} from '@components';

export const LoginScreen = ({navigation}) => {
  const [email, getEmail] = useState('');
  const [password, getPassword] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalMessage, setmodalMessage] = useState('');

  const loginButtonPressed = () => {
    if (email !== '' && password !== '') {
      navigation.navigate('HomeNavigation');
    } else {
      setmodalMessage(
        'Wprowadzono nieprawidłowy e-mail lub hasło. Spróbuj ponownie',
      );
      setModalVisible();
    }
  };

  return (
    <View style={{flex: 1}}>
      <RoundedHeader title="Fitly" desc="It's good to see you again" />
      <LoggingModal
        visible={modalVisible}
        setVisible={setModalVisible}
        values={modalMessage}
      />
      <LoginContentWrapper>
        <View>
          <LoginInput type="Email" getValues={getEmail} />
          <LoginInput type="Password" getValues={getPassword} />
        </View>
        <View>
          <RoundedButton
            text="Sign in"
            bgColor="#005b96"
            onPress={loginButtonPressed}
          />
          <LoginForgotPassword>
            <Text>Forgot Password?</Text>
          </LoginForgotPassword>
        </View>
        <SocialLogin />
        <RoundedButton
          text="Sign up"
          bgColor="#005b96"
          opacity={0.3}
          onPress={() => navigation.navigate('RegisterOptions')}
        />
      </LoginContentWrapper>
    </View>
  );
};

const LoginContentWrapper = styled.View`
  flex: 1;
  margin-vertical: 40px;
  margin-horizontal: 30px;
  justify-content: space-between;
`;
const LoginForgotPassword = styled.TouchableOpacity`
  align-items: center;
  margin-vertical: 20px;
`;
