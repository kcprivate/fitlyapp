import React from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  decrement,
  increment,
  incrementByAmount,
  count,
} from '../reducers/counterSlice';

const Test = () => {
  const counter = useSelector(count);
  const a = useSelector(state => state.counter.value);
  const dispatch = useDispatch();
  return (
    <View>
      <SafeAreaView>
        <Text>Hejka</Text>
        <Text>{counter}</Text>
        <TouchableOpacity
          style={{padding: 20, borderColor: 'red', borderWidth: 1}}
          onPress={() => dispatch(increment())}>
          <Text style={{fontSize: 25}}>Add</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{padding: 20, borderColor: 'red', borderWidth: 1}}
          onPress={() => dispatch(decrement())}>
          <Text style={{fontSize: 25}}>Remove</Text>
        </TouchableOpacity>
        <Text>{a}</Text>
      </SafeAreaView>
    </View>
  );
};

export default Test;
