import React from 'react';
import styled from 'styled-components';

export const ProductInput = ({
  title,
  onChange,
  kbdt,
  placeholder,
  containerStyle,
  textStyle,
  multiline,
  inputStyle,
}) => {
  return (
    <InputWrapper
      kbdt={kbdt}
      style={[{flex: kbdt !== 'default' ? 1 : 0}, containerStyle]}>
      <TitleWrapper style={textStyle}>{title}</TitleWrapper>
      <TextInputWrapper
        onChangeText={onChange}
        placeholder={placeholder}
        style={inputStyle}
        keyboardType={kbdt}
        multiline={multiline}
      />
    </InputWrapper>
  );
};

const InputWrapper = styled.View`
  margin-horizontal: ${p => (p.kbdt === 'default' ? 40 + 'px' : 5 + 'px')};
  margin-vertical: 10px;
`;

const TitleWrapper = styled.Text`
  text-align: ${p => (p.kbdt !== 'default' ? 'center' : 'left')};
  margin-bottom: 7px;
`;

const TextInputWrapper = styled.TextInput`
  background-color: white;
  padding-vertical: 15px;
  border-radius: 20px;
  padding-left: 20px;
`;
