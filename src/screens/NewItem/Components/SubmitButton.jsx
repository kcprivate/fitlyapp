import React from 'react';
import styled from 'styled-components';

export const SubmitButton = ({title, validation, containerStyle}) => {
  return (
    <ButtonWrapper style={containerStyle} onPress={validation}>
      <ButtonText>{title}</ButtonText>
    </ButtonWrapper>
  );
};

const ButtonWrapper = styled.TouchableOpacity`
  background-color: #5d71e4;
  margin-horizontal: 40px;
  padding-vertical: 20px;
  border-radius: 10px;
  margin-bottom: 30px;
`;

const ButtonText = styled.Text`
  text-align: center;
  color: white;
  font-size: 15px;
`;
