import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import styled from 'styled-components';

import {SearchTab} from './SearchTab';
import {ProductTab} from './ProductTab';
import {TrainingTab} from './TrainingTab';
import {ReceipesTab} from './ReceipesTab';

import Back from '@images/icons/back.svg';
import Search from '@images/icons/search.svg';

const TabNavigator = ({title, onPress, active}) => {
  return (
    <TabNavWrapper onPress={onPress} active={active}>
      <Text style={{color: active ? 'white' : '#96a6fe'}}>{title}</Text>
    </TabNavWrapper>
  );
};

export const NewItem = ({navigation}) => {
  const [index, setIndex] = useState(0);
  const [headerOpacity, setHeaderOpacity] = useState(1);
  const [searchData, setSearchData] = useState([
    {type: 'yesterday', mealType: 'Lunch', kcal: 800},
    {type: 'recent', mealType: 'Tuńczyk', description: '', kcal: 400},
    {
      type: 'recent',
      mealType: 'Tosty',
      description: 'z serem i szynką',
      kcal: 700,
    },
    {
      type: 'recent',
      mealType: 'Nuggetsy',
      description: 'z kurczaka + sos śmietanowy',
      kcal: 800,
    },
    {
      type: 'recent',
      mealType: 'Tosty',
      description: 'z serem i szynką',
      kcal: 700,
    },
    {
      type: 'recent',
      mealType: 'Nuggetsy',
      description: 'z kurczaka + sos śmietanowy',
      kcal: 800,
    },
    {
      type: 'recent',
      mealType: 'Tosty',
      description: 'z serem i szynką',
      kcal: 700,
    },
    {
      type: 'recent',
      mealType: 'Nuggetsy',
      description: 'z kurczaka + sos śmietanowy',
      kcal: 800,
    },
    {
      type: 'recent',
      mealType: 'Tosty',
      description: 'z serem i szynką',
      kcal: 700,
    },
    {
      type: 'recent',
      mealType: 'Nuggetsyaa',
      description: 'z kurczaka + sos śmietanowy',
      kcal: 800,
    },
  ]);

  const renderScene = index => {
    switch (index) {
      case 0:
        return <SearchTab data={searchData} />;
      case 1:
        return <ProductTab />;
      case 2:
        return <TrainingTab />;
      case 3:
        return <ReceipesTab setOpacity={setHeaderOpacity} />;
      default:
        break;
    }
  };

  return (
    <View style={{flex: 1}}>
      <HeaderWrapper opacity={headerOpacity}>
        <TopOfHeader>
          <TouchableOpacity
            style={{flex: 3}}
            onPress={() => navigation.goBack()}>
            <Back
              width={25}
              height={25}
              fill={'white'}
              style={{marginLeft: 20}}
            />
          </TouchableOpacity>
          <HeaderText>
            <Text style={{color: 'white'}}>Nowy wpis</Text>
          </HeaderText>
          <ThirdHeaderSpace />
        </TopOfHeader>
        <SearchBarWrapper>
          <Search width={30} height={30} />
          <SearchInput placeholder="Wyszukaj coś" />
        </SearchBarWrapper>
        <TabWrapper>
          <TabNavigator
            title="Szukaj"
            onPress={() => setIndex(0)}
            active={index === 0}
          />
          <TabNavigator
            title="Produkt"
            onPress={() => setIndex(1)}
            active={index === 1}
          />
          <TabNavigator
            title="Ćwiczenia"
            onPress={() => setIndex(2)}
            active={index === 2}
          />
          <TabNavigator
            title="Przepisy"
            onPress={() => setIndex(3)}
            active={index === 3}
          />
        </TabWrapper>
      </HeaderWrapper>
      <View style={{flex: 1}}>{renderScene(index, navigation)}</View>
    </View>
  );
};

const TabNavWrapper = styled.TouchableOpacity`
  padding-bottom: 20px;
  border-bottom-color: white;
  border-bottom-width: ${p => (p.active ? 2 + 'px' : 0 + 'px')};
`;

const HeaderWrapper = styled.View`
  background-color: #5d71e4;
  padding-top: 50px;
  border-bottom-left-radius: 70px;
  opacity: ${p => p.opacity};
`;

const TopOfHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const HeaderText = styled.View`
  flex: 3;
  align-items: center;
`;

const ThirdHeaderSpace = styled.View`
  width: 50px;
  flex: 3;
`;

const SearchBarWrapper = styled.View`
  margin-top: 20px;
  background-color: white;
  margin-horizontal: 30px;
  border-radius: 20px;
  justify-content: center;
  flex-direction: row;
  padding: 10px;
`;

const SearchInput = styled.TextInput`
  flex: 1;
  padding-left: 10px;
`;

const TabWrapper = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  margin-horizontal: 20px;
  align-items: center;
  margin-top: 20px;
`;
