import React, {useState} from 'react';
import {View, Alert} from 'react-native';
import styled from 'styled-components';

import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import {addTraining} from '@reducers/trainingStore';

import {ProductInput, SubmitButton} from './Components';

export const TrainingTab = () => {
  const [title, set__title] = useState('');
  const [time, set__time] = useState();
  const [calories, set__calories] = useState();

  const dispatch = useDispatch();
  const nav = useNavigation();

  const addValidation = () => {
    if (title !== '' && title && time && calories) {
      dispatch(
        addTraining({
          title,
          time,
          calories,
        }),
      );
      nav.reset({index: 0, routes: [{name: 'HomeNavigation'}]});
    } else {
      Alert.alert('Brak danych', 'Wprowadź wszystkie dane', [{text: 'Okejka'}]);
    }
  };

  return (
    <Wrapper>
      <View style={{marginTop: 20}}>
        <ProductInput
          title="Nazwa treningu"
          onChange={set__title}
          kbdt="default"
          placeholder="Wprowadź nazwę"
        />
        <DoubleInputs>
          <ProductInput
            title="Czas treningu"
            onChange={set__time}
            kbdt="default"
            placeholder="00:00 h"
            containerStyle={{flex: 1, marginRight: 10}}
            textStyle={{textAlign: 'center'}}
          />
          <ProductInput
            title="Spalone kalorie"
            onChange={set__calories}
            kbdt="default"
            placeholder="0 kcal"
            containerStyle={{flex: 1, marginLeft: 10}}
            textStyle={{textAlign: 'center'}}
          />
        </DoubleInputs>
      </View>
      <SubmitButton validation={addValidation} title="Dodaj ćwiczenie" />
    </Wrapper>
  );
};

const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
`;

const DoubleInputs = styled.View`
  flex-direction: row;
  justify-content: center;
`;
