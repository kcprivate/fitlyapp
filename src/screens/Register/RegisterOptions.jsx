import React, {useState} from 'react';
import {View, SafeAreaView, Text, TouchableOpacity} from 'react-native';

import styled from 'styled-components';

import {
  LoggingModal,
  GoalOption,
  SexOption,
  AgeOption,
  HeightOption,
  WeightOption,
  CountWeeks,
} from '@components';

import Back from '@images/icons/back.svg';

const RenderContent = props => {
  switch (props.goalCount) {
    case 1:
      return <GoalOption {...props} />;
    case 2:
      return <SexOption {...props} />;
    case 3:
      return <AgeOption {...props} />;
    case 4:
      return <HeightOption {...props} />;
    case 5:
      return <WeightOption {...props} />;
    case 6:
      return <CountWeeks {...props} />;
    default:
      return null;
  }
};

export const RegisterOptions = ({navigation}) => {
  const [registerOptions, setRegisterOptions] = useState([]);
  const [goalCount, setGoalCount] = useState(1);
  const [maxGoals, setMaxGoals] = useState(6);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [selected, setSelected] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalMessage, setModalMessage] = useState(false);

  const navigationButtonPress = () => {
    if (selected !== '') {
      let tempArray = registerOptions;
      tempArray.push(selected);
      setRegisterOptions(tempArray);
      setSelected('');
      if (goalCount === maxGoals) {
        navigation.navigate('RegisterForm', {data: registerOptions});
      } else {
        setGoalCount(goalCount + 1);
      }
    } else {
      setModalMessage('Wybierz jedną z opcji');
      setModalVisible();
    }
  };

  const backButtonPress = () => {
    goalCount === 1 ? navigation.goBack() : setGoalCount(goalCount - 1);
    setSelected('');
    let tempArray = registerOptions;
    tempArray.pop();
    setRegisterOptions(tempArray);
  };

  return (
    <OptionsWrapper>
      <LoggingModal
        visible={modalVisible}
        setVisible={setModalVisible}
        values={modalMessage}
      />
      <SafeAreaView style={{flex: 1}}>
        <HeaderWrapper>
          <View style={{width: '100%'}}>
            <Text style={{color: 'gray'}}>
              {goalCount} of {maxGoals}
            </Text>
            <Title>{title}</Title>
            {description !== '' && <Desc>{description}</Desc>}
          </View>
          <RenderContent
            goalCount={goalCount}
            setDescription={setDescription}
            setTitle={setTitle}
            selected={selected}
            setSelected={setSelected}
          />
        </HeaderWrapper>
      </SafeAreaView>
      <NavigationBar
        style={{
          shadowOffset: {
            width: 0,
            height: 2,
          },
          elevation: 4,
        }}>
        <TouchableOpacity style={{padding: 5}} onPress={backButtonPress}>
          <Back
            width={25}
            height={25}
            fill={'black'}
            strokeWidth="20"
            stroke="black"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={navigationButtonPress}>
          <NavigationButton>
            {goalCount < maxGoals ? 'Continue' : 'Finish'}
          </NavigationButton>
        </TouchableOpacity>
      </NavigationBar>
    </OptionsWrapper>
  );
};

const OptionsWrapper = styled.View`
  flex: 1;
  background-color: #f2f2f2;
`;

const HeaderWrapper = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-horizontal: 30px;
`;

const Title = styled.Text`
  color: #005b96;
  font-size: 30px;
  font-weight: 600;
  margin-top: 10px;
`;

const Desc = styled.Text`
  color: #777777;
  font-size: 15px;
  font-weight: 400;
  margin-top: 10px;
`;

const NavigationBar = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-horizontal: 30px;
  background-color: white;
  padding-vertical: 30px;
  border-top-right-radius: 50px;
  shadow-color: #000;
  shadow-opacity: 0.23;
  shadow-radius: 2.62px;
`;

const NavigationButton = styled.Text`
  color: #005b96;
  font-size: 15px;
`;
