import React, {useState} from 'react';
import {View, Alert} from 'react-native';

import styled from 'styled-components';

import {
  RoundedHeader,
  LoginInput,
  RoundedButton,
  SocialLogin,
} from '@components';

export const RegisterForm = props => {
  const [email, getEmail] = useState('');
  const [password, getPassword] = useState('');
  const [firstName, getFirstName] = useState('');
  const [lastName, getLastName] = useState('');

  const validation = () => {
    const {data} = props.route.params;
    if (
      email === '' ||
      password === '' ||
      firstName === '' ||
      lastName === ''
    ) {
      let empty = 'Brakuje pola: ';

      email === '' && (empty += '\nEmail');
      password === '' && (empty += '\nPassword');
      firstName === '' && (empty += '\nFirst Name');
      lastName === '' && (empty += '\nLast Name');

      Alert.alert('Prosimy o uzupełnienie wszystkich danych', empty, [
        {text: 'OK'},
      ]);
    } else {
      let registerData = {
        email: email,
        password: password,
        firstName: firstName,
        lastName: lastName,
      };

      let choosedOptions = {
        goal: data[0],
        sex: data[1] === 0 ? 'male' : 'female',
        age: data[2],
        height: data[3],
        weight: data[4],
        goalTime: data[5],
      };

      props.navigation.navigate('HomeNavigation', {
        choosedOptions,
        registerData,
      });
    }
  };

  return (
    <View>
      <RoundedHeader
        title="Register to see your nutrition plan"
        size="30px"
        weight={400}
      />
      <InputsWrapper>
        <LoginInput type="Email" getValues={getEmail} />
        <NamesInputWrapper>
          <LoginInput half type="First Name" getValues={getFirstName} />
          <LoginInput half type="Second Name" getValues={getLastName} />
        </NamesInputWrapper>
        <LoginInput type="Password" getValues={getPassword} />
      </InputsWrapper>
      <View style={{paddingHorizontal: 30}}>
        <RoundedButton text="Sign Up" bgColor="#005b96" onPress={validation} />
        <SocialLogin style={{marginTop: 40}} />
      </View>
    </View>
  );
};

const InputsWrapper = styled.View`
  padding-horizontal: 30px;
  margin-top: 50px;
  margin-bottom: 20px;
`;

const NamesInputWrapper = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: center;
`;
