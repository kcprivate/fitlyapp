import React from 'react';
import {View} from 'react-native';

import store from '@configs/store';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore} from 'redux-persist';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {
  HomeScreen,
  FoodScreen,
  TrainingScreen,
  UserScreen,
} from '@screens/Home';

import {WelcomeScreen} from '@screens/Welcome';
import {LoginScreen} from '@screens/Login';
import {RegisterOptions} from '@screens/Register';
import {RegisterForm} from '@screens/Register';
import {NewItem} from '@screens/NewItem';

import NoteIcon from '@images/icons/noteIcon.svg';
import UserIcon from '@images/icons/userIcon.svg';
import FoodIcon from '@images/icons/foodIcon.svg';
import RunIcon from '@images/icons/runIcon.svg';
import AddCircle from '@images/icons/add__circle.svg';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const HomeNavigation = () => {
  const iconRender = (type, isFocused, extraOptions) => {
    let ComponentType = null;

    switch (type) {
      case 'Home':
        ComponentType = NoteIcon;
        break;
      case 'Trainings':
        ComponentType = RunIcon;
        break;
      case 'Food':
        ComponentType = FoodIcon;
        break;
      case 'User':
        ComponentType = UserIcon;
        break;
      // case 'Add':
      //   ComponentType = ;
      //   break;
      default:
        return null;
    }

    return (
      <View style={extraOptions}>
        <ComponentType
          width={30}
          height={30}
          fill={isFocused ? '#005b96' : '#111'}
        />
      </View>
    );
  };

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
          padding: 10,
        },
      }}
      initialRouteName="Home">
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({focused}) => iconRender('Home', focused),
          tabBarShowLabel: false,
        }}
      />
      <Tab.Screen
        name="Trainings"
        component={TrainingScreen}
        options={{
          tabBarIcon: ({focused}) => iconRender('Trainings', focused),
          tabBarShowLabel: false,
        }}
      />
      <Tab.Screen
        name="Add"
        component={NewItem}
        options={{
          tabBarIcon: ({focused}) => (
            <AddCircle
              width={60}
              height={60}
              fill={focused ? '#00568a' : 'black'}
              style={{marginTop: 5}}
            />
          ),
          tabBarShowLabel: false,
        }}
      />
      <Tab.Screen
        name="Food"
        component={FoodScreen}
        options={{
          tabBarIcon: ({focused}) => iconRender('Food', focused),
          tabBarShowLabel: false,
        }}
      />
      <Tab.Screen
        name="User"
        component={UserScreen}
        options={{
          tabBarIcon: ({focused}) => iconRender('User', focused),
          tabBarShowLabel: false,
        }}
      />
    </Tab.Navigator>
  );
};

let persistor = persistStore(store);

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <View style={{flex: 1}}>
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{headerShown: false}}
              initialRouteName="HomeNavigation">
              <Stack.Group>
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Group>
                  <Stack.Screen
                    name="RegisterOptions"
                    component={RegisterOptions}
                  />
                  <Stack.Screen name="RegisterForm" component={RegisterForm} />
                </Stack.Group>
              </Stack.Group>
              <Stack.Screen name="Welcome" component={WelcomeScreen} />
              <Stack.Screen name="HomeNavigation" component={HomeNavigation} />
            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </PersistGate>
    </Provider>
  );
};

export default App;
